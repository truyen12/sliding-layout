package crocodile8000.sliding;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;



/**
 * SlidingLay - "�����������" ������� � ��������� ������� ����� ����������
 * 
 * ���������� ������ layout, �������� ���������� � ������� TranslateAnimation, 
 * ��� ��������� onTouchEvent - ����������� ������� ������� .layout(...),
 * ��������� (��� ���) ������� �� �������� - � ������ onInterceptTouchEvent(...)
 * ������� ������ ������� - int maxX
 * @author Crocodile8008
 *
 */

public class SlidingLayout extends LinearLayout {
	
//	private final static String LOG = "SlidingLay";
	
	private int toleranceToMove, maxX, dividerXleft, dividerXright;
	
	private int tX, tXstart, tXinner, tYstart, xOld;
	private int layW, layH, layXcurr;
	
	private boolean isLeftSlide, canMove;
	private boolean dontSlideNow;
	
	private Animation movement, correcting;
	private Interpolator interpolator;

	private final static float CORR_SPEED = 0.002f;
	
	
	
	
	
	@SuppressLint("NewApi")
	public SlidingLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(); }

	public SlidingLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(); }

	public SlidingLayout(Context context) {
		super(context);
		init(); }

	
	
	
	
	public void init(){
		interpolator = new LinearInterpolator();
	}
	
	
	
	
	/**
	 * ������ ���������
	 */
	public void setDontSlide(){
		dontSlideNow = true; }
	
	/**
	 * ���������� ���������
	 */
	public void setCanSlide(){
		dontSlideNow = false; }
	
	
	
	
	
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout( changed,  l,  t,  r,  b);
		
		if (layW <1) layW = getWidth();
		if (layH <1) layH  = getHeight();
		
		toleranceToMove = layW/15;	// ����� ���������������� ��� ����������� ������������� ����������� �������
		maxX = layW - layW/3; 		// ������������ �������� ������ ������
		dividerXleft = maxX/2 /2; 	// ���������� ������� �� ������� ������ ������ � ������ ���������
		dividerXright = maxX/2 /4; 	// ���������� ������� �� ������� ������ ������ � ������ ���������
		layXcurr = this.getLeft();	// ������� � ������ ������������ ����� ������ (����� ����)
	}

	
	
	

	
	/**
	 * ��������� ������� �������� ����
	 */
	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		if (dontSlideNow) return false;
		if (event.getAction() != MotionEvent.ACTION_MOVE) canMove = true;
		isLeftSlide = false;
	    if (event.getAction() == MotionEvent.ACTION_DOWN) {
			canMove = false;
	    	tYstart = (int)event.getY();
	    	tXstart = (int)event.getX();
	    } 
	    else if (event.getAction() == MotionEvent.ACTION_MOVE) {
	    	// ����������, �� ����� ��� �������� ������ � ���� �� � �� ������� layouyt, 
	    	// ����� ��������� ��������� ������� ������� (boolean isLeftSlide)
	        if (
	        Math.abs(tYstart - event.getY()) > toleranceToMove 
	        || Math.abs(tXstart - event.getX()) > toleranceToMove) {
		        canMove = true;
		        if (Math.abs(tYstart - event.getY()) >  Math.abs(tXstart - event.getX())){
		        	isLeftSlide = false; }
		        else{
		        	isLeftSlide = true; 
					tXinner = (int)event.getX();
					xOld = (int)event.getX()-tXinner;	
		        }
	        }
	    } 
	    else if (event.getAction() == MotionEvent.ACTION_UP) {
	    	isLeftSlide = false;
	    }
	    return isLeftSlide;
	}

	
	
	
	
	
	/**
	 * ������� ��������� ���� �������
	 */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	if (dontSlideNow) return false;
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			canMove= true;
			tXinner = (int)event.getX();
			xOld = (int)event.getX()-tXinner;
		}
		tX = (int)event.getX()-tXinner;
		if(canMove){
			checkXLimits(false);
			movement = new TranslateAnimation(xOld , tX , 0 , 0);
			movement.setDuration(0);
			movement.setFillAfter(true);
		    this.startAnimation(movement);
		    xOld = tX;
		}
		if (event.getAction() == MotionEvent.ACTION_UP) {
			animateCorrection();
		}
		redrawPlease();
		return true;
    }
    
    
    
    
    
    
    /**
     * ������ �������� ������� ������� ������ �� ������� ����
     */
    private void animateCorrection(){
    	
		int xTarget = 0;
		if (needCross()) xTarget = layXcurr > maxX/2? -maxX : maxX;
		
		int duration = (int)( ( Math.abs((float)xTarget - (float)xOld)/ (float)maxX )/CORR_SPEED );
		if (duration < 0) duration = 0;
		
		correcting = new TranslateAnimation( xOld , xTarget , 0 , 0 );
		correcting.setDuration( duration );
		correcting.setFillAfter(true);
		correcting.setInterpolator(interpolator);
		correcting.setAnimationListener(new AnimationListener() {
			public void onAnimationStart(Animation animation) {}
			public void onAnimationRepeat(Animation animation) {}
			public void onAnimationEnd(Animation animation) {
				endTranslate();
			}
		});
	    this.startAnimation(correcting);
    }
    
    
    
    
    
    
    /**
     * ���������� ������������ - ��������� ��������, ��������������� ���������
     */
    private void endTranslate(){
		checkXLimits(true);
        this.layout((int)tX, 0, (int)(tX+layW), (int)layH);
        this.clearAnimation();
        redrawPlease();
        movement = null;
        correcting = null;
    }
    
    
    
    
    
    /**
     * �������� �������� ���������� �� ��������
     */
    public void redrawPlease(){
        this.invalidate();
        try{
        	View parent = (View) this.getParent();
        	if (parent != null){
        		parent.postInvalidate(); 
        	}
        }catch(Exception e){ }
    }
    
    
    
    
    
    /**
     * ����� �� ����������� �����������
     */
    private boolean needCross(){
    	boolean cross = false;
		if (layXcurr < maxX/2 ){
			if (tX > dividerXleft) cross = true; }
		else{
			if (tX < -dividerXright) cross = true; }
    	return cross;
    }
    
    
    
    
    
    
    /**
     * �������� �� �������� �� X �� ���������� ������� + ���������� �������
     */
    private void checkXLimits(boolean endPositionCheck){
    	
    	// ����������� � �� ����� ��������
		if (tX >= maxX) tX = maxX;
		if (layXcurr < maxX/2 && tX < 0) tX = 0;
		if (layXcurr > maxX/2 && tX > 0) tX = 0; 
		if (layXcurr > maxX/2 && tX < -maxX) tX = -maxX;
		
		// ������� ����� ����������
		if (endPositionCheck) {
			// ���� ��� ����� ��������� �� ������ ��������
			if (layXcurr < maxX/2){
				if (tX < dividerXleft) tX=0; // ������� ������� �� dividerX/2
				else tX = maxX;
			}
			// ������
			else{
				if (tX < -dividerXright) tX=0;
				else tX = maxX;
			}
		}
    }
    
    
    
    
    /**
     * ������ view ��� ������� �� ������� ���� �� ����� ��������� /
     * Set the view when you click on that layer will not move
     * @param view
     */
    public void setIgnoredSlidingView(View view){
    	view.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE){
					setDontSlide();
				}
				else{
					setCanSlide();
				}
				return false;
			}
		});
    }
    
}
