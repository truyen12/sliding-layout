package crocodile8000.sliding;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.FrameLayout;

public class FrameLayoutHelper extends FrameLayout {
	
	int x,y,w,h;
	int number;
	
	
	@SuppressLint("NewApi")
	public FrameLayoutHelper(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);  }
	public FrameLayoutHelper(Context context, AttributeSet attrs) {
		super(context, attrs); }
	public FrameLayoutHelper(Context context) {
		super(context); 
		Log.d(SlidingDoubleLayout.LOG, "FrameLayoutHelper");
	}
	
	
	public void setNumber(int num){
		number = num;
	}
	
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout( changed,  l,  t,  r,  b);
		
		w = getWidth();
		h = getHeight();

		x = this.getLeft();
		y = this.getTop();

		Log.d(SlidingDoubleLayout.LOG, "F L Helper number="+number+"  x="+x + "  y="+y+ "  w "+w+"  h "+h);
	}
}
