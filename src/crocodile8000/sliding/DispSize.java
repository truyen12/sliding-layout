package crocodile8000.sliding;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;

public class DispSize {
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static int dWidth(Context mContext){
	    int width=0;
	    WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    if(Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB){                   
	        Point size = new Point();
	        display.getSize(size);
	        width = size.x;
	    }
	    else{
	        width = display.getWidth();  // deprecated
	    }
	    return width;
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static int dHeight(Context mContext){
	    int height=0;
	    WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    if(Build.VERSION.SDK_INT>Build.VERSION_CODES.HONEYCOMB){               
	        Point size = new Point();
	        display.getSize(size);
	        height = size.y;
	    }else{          
	        height = display.getHeight();  // deprecated
	    }
	    return height;      
	}
	
}
