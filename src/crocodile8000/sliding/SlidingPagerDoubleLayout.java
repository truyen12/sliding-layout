package crocodile8000.sliding;

import java.util.ArrayList;
import java.util.List;

import crocodile8000.slidingsample.R;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;



/**
 * SlidingPagerDoubleLayout - Double FrameLayouts + (extended)ViewPager � ������������ ��������� ���� � ��������
 * 
 * ���������� ������ layout, �������� ���������� � ������� TranslateAnimation, 
 * ��� ��������� onTouchEvent - ����������� ������� ������� .layout(...),
 * ��������� (��� ���) ������� �� �������� - � ������ onInterceptTouchEvent(...)
 * ������� ������ ������� - int maxX
 * @author Crocodile8008
 *
 */

public class SlidingPagerDoubleLayout extends FrameLayout {
	
//	public final static String LOG = "SlidingLay";
	
	public static final int STATE_MENU = -1;
	public static final int STATE_NORMAL = 0;
	private int state = STATE_NORMAL;
	
	private int toleranceToMove, maxX, dividerXleft, dividerXright;
	
	private int tX, tXtoMove, tXstart, tXinner, tYstart, xOld;
	private int layW, layH;
	
	private boolean isHorizontalMove, toleranceOver;
	private boolean dontSlideNow;
	
	private Animation movement, correcting;
	private Interpolator interpolator;

	private final static float CORR_SPEED = 0.002f;
	
	public FrameLayout screen;
	public FrameLayout leftMenu;
	public ExtendedPager viewPager;
	public List<View> pages;
	public PagerTabStrip pagerTab;
	
	
	
	
	/*
	@SuppressLint("NewApi")
	public SlidingPagerDoubleLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(); }

	public SlidingPagerDoubleLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(); }
	 */
	
	public SlidingPagerDoubleLayout(Context context, View mainView, View menuView) {
		super(context);
		init();
		initBaseView(mainView, menuView);
	}

	private void init(){
		interpolator = new LinearInterpolator();
	}
	
	private void initBaseView(View mainView, View menuView){
		leftMenu = new FrameLayout(getContext());
		leftMenu.addView(menuView);
		this.addView(leftMenu);
		
		screen = new FrameLayout(getContext());
		this.addView(screen);
		
		setIgnoredSlidingView(leftMenu);
		
		pages = new ArrayList<View>();
		
        pages.add(mainView);

        LayoutInflater li = LayoutInflater.from(getContext());
        SamplePagerAdapter pagerAdapter = new SamplePagerAdapter(pages);

        viewPager = (ExtendedPager) li.inflate(R.layout.lay_pager, null	);
   /*     
        viewPager = new ExtendedPager(getContext());
        pagerTab = new PagerTabStrip(getContext());
        pagerTab.setBackgroundColor(Color.argb(100, 0, 0, 0));
        pagerTab.setGravity(Gravity.TOP);
        pagerTab.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 100));
        viewPager.addView(pagerTab);
     */   
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(0);    
        viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			public void onPageSelected(int arg0) {
				hideOrNotMenu();
			}
			public void onPageScrolled(int arg0, float arg1, int arg2) {}
			public void onPageScrollStateChanged(int arg0) {}
		});
        
        screen.addView( viewPager );
	}
	
	
	
	/**
	 * �������� ��� ���� �������� ��� ��������������
	 * @param newView
	 */
	public void addOneMorePage(View newView){
		pages.add(newView);
		SamplePagerAdapter pagerAdapter = new SamplePagerAdapter(pages);
		viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(0);     
	}
	

	
	
	
	/**
	 * ������ ���������
	 */
	public void setDontSlide(){
		viewPager.dontSlideNow = true;
		dontSlideNow = true; }
	
	/**
	 * ���������� ���������
	 */
	public void setCanSlide(){
		viewPager.dontSlideNow = false;
		dontSlideNow = false; }
	
	/**
	 * ����� ������� ���������
	 * @return -1: menu / 0: normal state
	 */
	public int getCurrentState(){
		return state;
	}
	
	/**
	 * ����� ������� ��������
	 */
	public int getCurrentPage(){
		return viewPager.getCurrentItem();
	}
	
	/**
	 * ���������� ������� ��������
	 */
	public void setCurrentPage(int i){
		viewPager.setCurrentItem(i);
	}
	
	
	
	
	
	
	
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout( changed,  l,  t,  r,  b);
		
		if (layW <1) layW = getWidth();
		if (layH <1) layH  = getHeight();
		
		toleranceToMove = layW/20;	// ����� ���������������� ��� ����������� ������������� ����������� �������
		maxX = layW - layW/3; 		// ������������ �������� ������ ������
		dividerXleft = maxX/4; 	// ���������� ������� �� ������� ������ ������ � ������ ���������
		dividerXright = maxX/4; 	// ���������� ������� �� ������� ������ ������ � ������ ���������
		
		setState();
	}

	
	private void setState(){
		if (screen.getLeft() > layW/2) {
			state = STATE_MENU;
			viewPager.dontSlideNow = true;
		}else {
			state = STATE_NORMAL;
			viewPager.dontSlideNow = false;}
	}
	
	

	
	
	
	
	//TODO
	/**
	 * ��������� ������� �������� ����
	 */
	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		
		if (dontSlideNow) return false;

	    if (event.getAction() == MotionEvent.ACTION_DOWN) {
			toleranceOver = false;
			isHorizontalMove = false;
	    	tYstart = (int)event.getY();
	    	tXstart = (int)event.getX();
	    } 
	    
		if (state == STATE_MENU && tXstart < maxX) {
			return false;
		}
		
	    else if (event.getAction() == MotionEvent.ACTION_MOVE) {
	    	// ����������, �� ����� ��� �������� ������ � ���� �� � �� ������� layouyt, 
	    	// ����� ��������� ��������� ������� ������� (boolean isHorizontalMove)
	    	if (!toleranceOver){
		        if (
		        Math.abs(tYstart - event.getY()) > toleranceToMove  || Math.abs(tXstart - event.getX()) > toleranceToMove) {
		        	toleranceOver = true;
		        }
		    }
	    	else{
	    		if (state == STATE_NORMAL && tXstart > event.getX() || viewPager.getCurrentItem()>0){
	    			isHorizontalMove = false;
	    			return false;
	    		}
		        if ( Math.abs(tYstart - event.getY()) >  Math.abs(tXstart - event.getX()) ){
		        	isHorizontalMove = false; }
		        else{
		        	isHorizontalMove = true; 
					tXinner = (int)event.getX();
					xOld = (int)event.getX()-tXinner;	
		        }
	        }
	    } 
	    else if (event.getAction() == MotionEvent.ACTION_UP) {
	    	isHorizontalMove = false;
	    	toleranceOver = false;
	    }
	    return isHorizontalMove;
	}

	
	
	
	
	
	
	
	
	//TODO
	/**
	 * ������� ��������� ���� �������
	 */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	
    	if (dontSlideNow) return false;
    	
		tX = (int)event.getX() ;
		tXtoMove = tX - tXinner;
		checkXLimits(false);
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			toleranceOver= true;
			tXinner = (int)event.getX();
			xOld = tXtoMove;
		}
		if(toleranceOver){
			movement = new TranslateAnimation(xOld , tXtoMove , 0 , 0);
			movement.setDuration(0);
			movement.setFillAfter(true);
			screen.startAnimation(movement);
		    xOld = tXtoMove;
		}
		if (event.getAction() == MotionEvent.ACTION_UP) {
			animateCorrection();
		}
		redrawPlease();
		return true;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * ������ �������� ������� ������� ������ �� ������� ����
     */
    private void animateCorrection(){
    	
		int xTarget = 0;
		if (needCross()) xTarget = screen.getLeft() > maxX/2? -maxX : maxX;
		
		int duration = (int)( ( Math.abs((float)xTarget - (float)xOld)/ (float)maxX )/CORR_SPEED );
		if (duration < 0) duration = 0;
		
		correcting = new TranslateAnimation( xOld , xTarget , 0 , 0 );
		correcting.setDuration( duration );
		correcting.setFillAfter(true);
		correcting.setInterpolator(interpolator);
		correcting.setAnimationListener(new AnimationListener() {
			public void onAnimationStart(Animation animation) {}
			public void onAnimationRepeat(Animation animation) {}
			public void onAnimationEnd(Animation animation) {
				endTranslate();
			}
		});
		screen.startAnimation(correcting);
    }
    
    
    
    
    
    
    /**
     * ���������� ������������ - ��������� ��������, ��������������� ���������
     */
    private void endTranslate(){
		checkXLimits(true);
		screen.layout((int)tXtoMove, 0, (int)(tXtoMove+layW), (int)layH);
        screen.clearAnimation();
        redrawPlease();
        setState();
        movement = null;
        correcting = null;
    }
    
    
    
    
    
    /**
     * �������� ���������� �� ��������
     */
    public void redrawPlease(){
        this.invalidate();
    }
    
    
    
    
    
    /**
     * ����� �� ����������� �����������
     */
    private boolean needCross(){
    	boolean cross = false;
		if (screen.getLeft() < maxX/2 ){
			if (tXtoMove > dividerXleft) cross = true; }
		else{
			if (tXtoMove < -dividerXright) cross = true; }
    	return cross;
    }
    
    
    
    
    
    
    /**
     * �������� �� �������� �� X �� ���������� ������� + ���������� �������
     */
    private void checkXLimits(boolean endPositionCheck){
    	
	    	// ����������� � �� ����� ��������
			if (tX >= maxX) tX = maxX;
			
			// ������� ����� ����������
			if (endPositionCheck) {
				// ���� ��� ����� ��������� �� ������ ��������
				if (state == STATE_NORMAL){
					if (tXtoMove < dividerXleft) tXtoMove=0;
					else tXtoMove = maxX;
				}
				// ������
				else{
					if (tXtoMove < -dividerXright) tXtoMove= 0;
					else tXtoMove = maxX;
				}
			} 
    	if (endPositionCheck) {
	    	if (tXstart>0 && tX < tXstart-layW/2){
	    		screen.layout(-layW, 0, 0, layH);
	    		screen.layout(0, 0, layW, layH);
	    	}
    	}
    	hideOrNotMenu();
    }
    
    
    
    
    
    
    private void hideOrNotMenu(){
		if (viewPager.getCurrentItem()>0) leftMenu.setVisibility(View.GONE);
		else leftMenu.setVisibility(View.VISIBLE);
    }
    
    
    
    
    
    
    /**
     * ������ view ��� ������� �� ������� ���� �� ����� ��������� /
     * Set the view when you click on that layer will not move
     * @param view
     */
    public void setIgnoredSlidingView(View view){
    	try{
	    	view.setOnTouchListener(new OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE){
						setDontSlide();
					}else{
						setCanSlide();
					}
					return false;
				}
			});
    	}catch(Exception e){
  //  		Log.e(LOG, ""+e);
    	}
    }
    
}

