package crocodile8000.sliding;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

/**
 * ���������� ViewPager ��� ��������� ������� 
 * �� �������� ����� ����� (�� �������������� �������)
 * @author Crocodile8008
 *
 */
public class ExtendedPager extends ViewPager{
	
	public boolean dontSlideNow;
	
	public ExtendedPager(Context context) {
		super(context); }
	public ExtendedPager(Context context, AttributeSet attrs) {
		super(context, attrs); }

	public boolean onInterceptTouchEvent(MotionEvent event) {
		if (dontSlideNow) return false;
		return super.onInterceptTouchEvent(event);
	}
	
    public boolean onTouchEvent(MotionEvent event) {
  //  	if (dontSlideNow) return false;
		return super.onTouchEvent(event);
	}
    
}