package crocodile8000.slidingsample;

import java.util.Random;

import crocodile8000.sliding.*;

import android.os.Bundle;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityStartDoubleLayouts extends FragmentActivity {

	SlidingPagerDoubleLayout sl;
	Random rnd;

	private final static String SAVED_PAGE = "page";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		rnd = new Random();

		LayoutInflater li = LayoutInflater.from(this);
		
		// ������� ��������� SlidingPagerDoubleLayout � ������ �� �����
		sl = new SlidingPagerDoubleLayout(getApplicationContext(), li.inflate(R.layout.lay_main, null), li.inflate(R.layout.lay_menu, null));
		setContentView(sl);
		
		// ��������� �������� � SlidingPagerDoubleLayout
		sl.addOneMorePage(li.inflate(R.layout.lay_main2, null));
		sl.addOneMorePage(li.inflate(R.layout.lay_main2, null));
		sl.addOneMorePage(li.inflate(R.layout.lay_main2, null));
		
		// ��������� View, ������� �� ���� ������������ ����������
		sl.setIgnoredSlidingView(sl.pages.get(0).findViewById(R.id.scrollViewHorizontal));
		sl.setIgnoredSlidingView(sl.pages.get(1).findViewById(R.id.scrollViewHorizontal));
		sl.setIgnoredSlidingView(sl.pages.get(2).findViewById(R.id.scrollViewHorizontal));
		sl.setIgnoredSlidingView(sl.pages.get(3).findViewById(R.id.scrollViewHorizontal));
		
		// ��������� ������ �� ���������
		TextView 
		tv = (TextView)sl.pages.get(1).findViewById(R.id.tvTitle);
		tv.setText("Layout 2 / "+sl.pages.size());
		tv = (TextView)sl.pages.get(2).findViewById(R.id.tvTitle);
		tv.setText("Layout 3 / "+sl.pages.size());
		tv = (TextView)sl.pages.get(3).findViewById(R.id.tvTitle);
		tv.setText("Layout 4 / "+sl.pages.size());
		
	}
	
	
	
	public void onRestoreInstanceState(Bundle inState){
		super.onRestoreInstanceState(inState);
		sl.setCurrentPage(inState.getInt(SAVED_PAGE));
	}

	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    outState.putInt(SAVED_PAGE, sl.getCurrentPage() );
	}
	
	
	
	
	public void clickBt1(View v){
		for(int i=0; i< sl.pages.size(); i++)
			sl.pages.get(i).setBackgroundColor(Color.rgb(rnd.nextInt(250), rnd.nextInt(250), rnd.nextInt(250)));
	}

	
	public void clickBt2(View v){
		Toast.makeText(getApplicationContext(), "It's Sliding panel", Toast.LENGTH_SHORT).show();
	}
	
	
	public void about(View v){
		Toast.makeText(getApplicationContext(), "My e-mail: crocodile8000@gmail.com \nor: Andrey-r.com", Toast.LENGTH_LONG).show();
	}

}
